{{/*
A deployment that runs the Huey consumer using the 'run_huey' manage command.
This container will consume and process any tasks queued by other processes in a shared volume.
*/}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "dgh.backgroundSchedulerName" . }}
  labels:
    {{- include "dgh.standardLabels" . | nindent 4 }}
  {{- with .Values.schedulerAnnotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ .Values.schedulerReplicaCount }}
  selector:
    matchLabels:
      {{- include "dgh.selectorLabels" . | nindent 6 }}
      component: scheduler
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
    type: RollingUpdate
  template:
    metadata:
      name: {{ include "dgh.backgroundSchedulerName" . }}
      labels:
        {{- include "dgh.standardLabels" . | nindent 8 }}
        include-metrics: 'true'
        component: scheduler
    spec:
      containers:
        - name: {{ include "dgh.backgroundSchedulerName" . }}
          image: "{{ .Values.imageRepository }}:{{ .Values.imageTag }}"
          imagePullPolicy: IfNotPresent
          workingDir: /app/src
          command:
            - python
            - manage.py
            - run_huey
          env:
            - name: KEYCLOAK_ENABLED
              value: "False"
            - name: DEPLOYMENT_NAMESPACE
              value: "{{ .Release.Namespace }}"
            - name: PROMETHEUS_MULTIPROC_DIR
              value: /tmp/prometheus
          envFrom:
            - configMapRef:
                name: {{ include "dgh.name" . }}
            - secretRef:
                name: {{ include "dgh.name" . }}
          {{- if .Values.metrics.enabled }}
          ports:
          - name: metrics
            containerPort: {{ .Values.metrics.port }}
            protocol: TCP
         {{- end }}
          volumeMounts:
            - name: huey
              mountPath: /app/huey_data
            - name: file-uploads
              mountPath: /file-uploads
            - name: prometheus-tmp
              mountPath: /tmp/prometheus
          resources:
            {{- toYaml .Values.schedulerResources | nindent 12 }}
          {{- if .Values.adminSecurityContext }}
          securityContext:
            {{- toYaml .Values.adminSecurityContext | nindent 12 }}
          {{- end }}
      volumes:
        - name: huey
          persistentVolumeClaim:
            claimName: {{ include "dgh.hueySharedStoragePvcName" . }}
        - name: file-uploads
          persistentVolumeClaim:
            claimName: {{ include "dgh.fileUploadsSharedStoragePvcName" . }}
        - name: prometheus-tmp
          emptyDir: {}
      {{- if .Values.adminPodSecurityContext }}
      securityContext:
        {{- toYaml .Values.adminPodSecurityContext | nindent 8 }}
      {{- end }}
