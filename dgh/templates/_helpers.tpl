{{/*
The name of the release, used as label or a base for other names.
*/}}
{{- define "dgh.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{/*
The name used to labels resources associated with the web deployment.
*/}}
{{- define "dgh.webName" -}}
{{- printf "%s-web" (include "dgh.name" . )  -}}
{{- end }}

{{/*
The name used to labels resources associated with the admin deployment.
*/}}
{{- define "dgh.adminName" -}}
{{- printf "%s-admin" (include "dgh.name" . )  -}}
{{- end }}

{{/*
The name used to labels resources associated with the mock IDP deployment.
*/}}
{{- define "dgh.mockIdpName" -}}
{{- printf "%s-mock-idp" (include "dgh.name" . )  -}}
{{- end }}

{{/*
The name used to labels resources associated with the NGINX webserver deployment.
*/}}
{{- define "dgh.webserverName" -}}
{{- printf "%s-nginx" (include "dgh.name" . ) -}}
{{- end }}

{{/*
The name used to labels resources associated with the mock NGINX webserver deployment.
*/}}
{{- define "dgh.mockWebserverName" -}}
{{- printf "%s-mock-nginx" (include "dgh.name" . ) -}}
{{- end }}

{{/*
The name of the secret containing the keypair used to encrypt SAML traffic.
Through our metadata endpoint, this certificate is provided to external parties.
*/}}
{{- define "dgh.keypairSecretName" -}}
{{- printf "%s-keypair" (include "dgh.name" . ) -}}
{{- end }}

{{/*
The root CA to trust for the external API endpoint related to the BSN checker and other APIs.
*/}}
{{- define "dgh.rootCaSecretName" -}}
{{- printf "%s-root-ca" (include "dgh.name" . ) -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing media files, i.e. images and documents.
*/}}
{{- define "dgh.mediaSharedStoragePvcName" -}}
{{- printf "%s-media" (include "dgh.name" . ) -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing file uploads, i.e. files uploaded by visitors.
*/}}
{{- define "dgh.fileUploadsSharedStoragePvcName" -}}
{{- printf "%s-file-uploads" (include "dgh.name" . ) -}}
{{- end }}


{{/*
The name of the Persistent Volume Claim containing encrypted files.
*/}}
{{- define "dgh.encryptedFilesSharedStoragePvcName" -}}
{{- printf "%s-encrypted-files" (include "dgh.name" . ) -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing Huey related data.
*/}}
{{- define "dgh.hueySharedStoragePvcName" -}}
{{- printf "%s-huey" (include "dgh.name" . ) -}}
{{- end }}

{{/*
A set of standard labels as defined by the Helm developers to use in a chart.
*/}}
{{- define "dgh.standardLabels" -}}
{{ include "dgh.selectorLabels" . }}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
{{- end }}

{{- define "dgh.selectorLabels" -}}
app.kubernetes.io/name: {{ include "dgh.name" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "dgh.backgroundSchedulerName" -}}
{{- printf "%s-background-scheduler" (include "dgh.name" .) -}}
{{- end -}}
