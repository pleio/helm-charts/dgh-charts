# dgh

![Version: 0.4.1](https://img.shields.io/badge/Version-0.4.1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

A Helm chart for the DGH application stack to be run on Kubernetes

## Source Code

* <https://gitlab.com/pleio/helm-charts/dgh-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| admin.livenessProbe.failureThreshold | int | `6` | Number of failed liveness probes before container is restarted |
| admin.livenessProbe.initialDelaySeconds | int | `40` | Number of seconds to delay liveness probe start after container startup |
| admin.livenessProbe.periodSeconds | int | `15` | Interval (in seconds) between liveness probes |
| admin.livenessProbe.successThreshold | int | `1` | Number of successful probes required before the container is marked as live |
| admin.livenessProbe.timeoutSeconds | int | `5` | Timeout in seconds for each liveness probe response |
| admin.readinessProbe.failureThreshold | int | `6` | Number of failed readiness probes before container is marked as not ready |
| admin.readinessProbe.initialDelaySeconds | int | `20` | Number of seconds to delay readiness probe start after container startup |
| admin.readinessProbe.periodSeconds | int | `15` | Interval (in seconds) between readiness probes |
| admin.readinessProbe.successThreshold | int | `1` | Number of successful probes required before the container is marked as ready |
| admin.readinessProbe.timeoutSeconds | int | `5` | Timeout in seconds for each readiness probe response |
| adminDeploymentAnnotations | object | `{}` | Annotations to apply to the admin deployment. |
| adminDomain | string | `"chart-example.local"` | The domain used by admins and content creators to navigate to the Wagtail CMS and Django Admin of the application. |
| adminKeycloakEnabled | string | `"True"` | Controls if Keycloak is enabled for the admin container |
| adminPodSecurityContext | object | `{}` | Pod-level security context for admin pods. Define security settings such as runAsUser, fsGroup, etc. |
| adminReplicaCount | int | `1` | Amount of replicas used for the DGH admin deployment. |
| adminResources | object | `{}` | Resource requests for the DGH main admin container. Left empty intentionally. Set according to the target environment. |
| adminSecurityContext | object | `{}` | Security context for admin containers. Define container-level security settings such as capabilities, privileged, etc. |
| adminWhitelist | string | `"127.0.0.1"` | The allow list to control who can access the Django Admin and Wagtail CMS pages. |
| applyFixtures | bool | `false` | A toggle to clean the database and install the fixtures. Only use in non-production environments. |
| encryptedFilesSharedStorageSize | string | `"1Gi"` | The size of the shared storage designated for encrypted files. |
| encryptedFilesStorage | string | `""` | The file system path to store encrypted files. |
| env | string | `"none"` | Setting the ENV variable equal to "test" will run the application in DEBUG mode. Only use for non-production environments. |
| externalApiHost | string | `"chart-example.local"` | The host name of an external API to make retrieve data for a user by providing their BSN number. |
| externalLoadbalancerDomain | string | `""` | The domain used by external load balancers to send traffic to. This is primarily in place to facilitate DigiD authentication. |
| externalLoadbalancerWhitelist | string | `""` | The allow list for the domain used by external load balancers. This should only allow the specific load balancers configured to forward traffic to our servers. |
| fileUploadsSharedStorageSize | string | `"1Gi"` | Size of the shared storage for file uploads. |
| fromEmail | string | `""` | Default email address to use for various automated correspondence from the site manager(s). |
| hueySharedStorageSize | string | `"1Gi"` | The size of the shared storage designated for the Huey task scheduler. |
| imageRepository | string | `"registry.gitlab.com/pleio/dossier-dgh"` | Default image repository for the DGH application. |
| imageTag | string | `"0.1.0"` | Default DGH application image tag to use. |
| maintenance | string | `"False"` | A toggle used to display a maintenance page for each request. Set to "True" to enable, otherwise "False". |
| mediaSharedStorageSize | string | `"1Gi"` | The size of the shared storage designated for media files, i.e. images and documents. |
| mediaStorage | string | `""` | The file system path to store media related to the application. |
| metrics.enabled | bool | `false` |  |
| metrics.port | int | `9090` |  |
| metrics.serviceMonitor.enabled | bool | `false` |  |
| metrics.serviceMonitor.interval | string | `"10s"` |  |
| mockBsnChecker | bool | `false` | A toggle to turn the mock API on or off. Only use in non-production environments. |
| mockSamlConnection | bool | `false` | A toggle to turn the SimpleSAML IDP on or off. Only use in non-production environments. |
| monitoringDomain | string | `"chart-example.local"` | The domain exposed to allow for the collection of logs and metrics. |
| monitoringWhitelist | string | `"127.0.0.1"` | The whitelist for the monitoring scraping endpoint. This should only allow the requests designated to collect logs and metrics. |
| nginxRepository | string | `"nginxinc/nginx-unprivileged"` | The image repository for the NGINX webserver. |
| nginxTag | string | `"1.27-alpine"` | The image tag for the NGINX webserver container. |
| samlEntityId | string | `""` | The SAML entity ID url where the SP published public information about itself. |
| samlMetadataUrl | string | `""` | The metadata url of the SAML Identity Provider used to exchange information regarding encryption and endpoints. |
| samlServiceId | string | `""` | The id of the service that can be consumed by login to this client. |
| schedulerAnnotations | object | `{}` | Annotations to apply to the background scheduler deployment. |
| schedulerReplicaCount | int | `1` | Default image repository for the DGH application. |
| schedulerResources | object | `{}` | Resource requests for the DGH scheduler containers (Huey consumers). Left empty intentionally. Set according to the target environment. |
| sesMailBackend | bool | `false` | Enable SES (Simple Email Service) as the mail backend. |
| sharedStorageClassName | string | `""` | The name of the Storage Class to be used for shared pod storage. Must be of access mode RWX. |
| trustedCertsLocation | string | `""` | The list of CA certificates used by the python 'requests' module. |
| web.livenessProbe.failureThreshold | int | `6` | Number of failed liveness probes before container is restarted |
| web.livenessProbe.initialDelaySeconds | int | `40` | Number of seconds to delay liveness probe start after container startup |
| web.livenessProbe.periodSeconds | int | `15` | Interval (in seconds) between liveness probes |
| web.livenessProbe.successThreshold | int | `1` | Number of successful probes required before the container is marked as live |
| web.livenessProbe.timeoutSeconds | int | `5` | Timeout in seconds for each liveness probe response |
| web.readinessProbe.failureThreshold | int | `6` | Number of failed readiness probes before container is marked as not ready |
| web.readinessProbe.initialDelaySeconds | int | `20` | Number of seconds to delay readiness probe start after container startup |
| web.readinessProbe.periodSeconds | int | `15` | Interval (in seconds) between readiness probes |
| web.readinessProbe.successThreshold | int | `1` | Number of successful probes required before the container is marked as ready |
| web.readinessProbe.timeoutSeconds | int | `5` | Timeout in seconds for each readiness probe response |
| webDeploymentAnnotations | object | `{}` | Annotations to apply to the web deployment. |
| webDomain | string | `"dgh.local"` | The main domain used by users to navigate to the internet-facing side of the application. |
| webKeycloakEnabled | string | `"False"` | Controls if Keycloak is enabled for the web container |
| webPodSecurityContext | object | `{}` | Pod-level security context for web pods. Define security settings such as runAsUser, fsGroup, etc. |
| webReplicaCount | int | `1` | Amount of replicas used for the DGH web deployment. |
| webResources | object | `{}` | Resource requests for the DGH main web container. Left empty intentionally. Set according to the target environment. |
| webSecurityContext | object | `{}` | Security context for web containers. Define container-level security settings such as capabilities, privileged, etc. |
| webWhitelist | string | `"chart-example.local"` | The allow list for the internet-facing portion of the application. Should not be used in production. |
| wildcardTlsSecretName | string | `""` | The name of the K8S secret containing the key pair for the wildcard certificate used. To obtain a LetsEncrypt certificate, set to a name not associated to any secret to allow cert-manager to request a certificate. |
