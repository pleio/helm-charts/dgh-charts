# DGH Charts

This repository contains the Helm-chart for [DGH](https://gitlab.com/pleio/dossier-dgh) (and any future charts which are related to this project).

## Installation
See [the DGH Chart's README](./dgh/README.md) for installation instructions

## Chart Releases
Any changes in the chart directories (e.g. [dgh](./dgh/)) triggers a pipeline which publishes a chart package into this project's [package registry](https://gitlab.com/pleio/helm-charts/dgh-charts/-/packages).

## Development
Please create merge-requests for any additional features or bugfixes. Please do not forget to:
* bump the chart-version in the Chart.yaml (so `version`, `appversion` can stay as is)
* add any additional values in de [DGH chart README](./dgh/README.md) and run [helm-docs](https://github.com/norwoodj/helm-docs/tree/master) to automatically generate the docs.
